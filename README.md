# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://nikbratusa@bitbucket.org/nikbratusa/stroboskop.git

Naloga 6.2.3:

https://bitbucket.org/nikbratusa/stroboskop/commits/9edd62ff111120622d146f412108757472e363e7

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/nikbratusa/stroboskop/commits/719398a8b1b7cffaf7b998d5f46d196120722c32

Naloga 6.3.2:
https://bitbucket.org/nikbratusa/stroboskop/commits/6dc57a2962ce9bd09650490cd3503316afece0bf

Naloga 6.3.3:
https://bitbucket.org/nikbratusa/stroboskop/commits/b3b20651e36196a607e37a5c55512b62536f859e

Naloga 6.3.4:
https://bitbucket.org/nikbratusa/stroboskop/commits/099fef411232adf53e740cc9dd4ec508c9535355

Naloga 6.3.5:

git checkout master
git merge izgled

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/nikbratusa/stroboskop/commits/c6da668defa1449f77df75cf765a368601662a06

Naloga 6.4.2:
https://bitbucket.org/nikbratusa/stroboskop/commits/3ef91f1fb05b45c8bf6c951df3095fbcfa513754

Naloga 6.4.3:
https://bitbucket.org/nikbratusa/stroboskop/commits/b31d31416adfeba509eab0979a08f6bc8c476821

Naloga 6.4.4:
https://bitbucket.org/nikbratusa/stroboskop/commits/c4d2012d1188f2b4caf9d078453c23bfe3c8f437